<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

# RUST Security Code Review
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->


victorht | 2021


----  ----

# 1. RUST Programming Language Introduction

----

## RUST Introduction

> "Rust is a multi-paradigm programming language designed for __performance and safety__, especially safe concurrency. Rust is syntactically similar to C++, but can guarantee memory safety by using a borrow checker to validate references."
>
> -- <cite>https://www.rust-lang.org/</cite>

<div class="fragment" />

* Created by *Mozilla* and mainly used to build "system software" so it is known as system programming language
* <!-- .element: class="fragment" --> There are 2 editions [2015][E1] and [2018][E2]
* <!-- .element: class="fragment" --> A browser interface to the Rust compiler to experiment with the language. [Rust Playground] [E3]

[E1]: https://doc.rust-lang.org/edition-guide/rust-2015/index.html
[E2]: https://doc.rust-lang.org/edition-guide/rust-2018/index.html
[E3]: https://play.rust-lang.org/


----

## Performance

<div class="fragment" />

* Similar performance like C/C++
* <!-- .element: class="fragment" --> No garbage collector
* <!-- .element: class="fragment" --> Low runtime overhead

----

## 0-cost abstraction

<div class="fragment" />

* Inline memory layout
* <!-- .element: class="fragment" --> [Static dispatch][Z1] (it is fast but there is a tradeoff, 'code bloat')

[Z1]: https://doc.rust-lang.org/1.8.0/book/trait-objects.html


----

## Secure by design

<div class="fragment" />

* Strong type checking
* <!-- .element: class="fragment" --> Memory Ownership, borrow checking & Lifetimes
* <!-- .element: class="fragment" --> Compile time memory safety checks
* <!-- .element: class="fragment" --> Concurrency without data races
* <!-- .element: class="fragment" --> [etc1][S1], [etc2][S2], [etc3][S3], ... 

[S1]: https://google.com
[S2]: https://google.com
[S3]: https://google.com 


----  ----

# 2. Rust Features (with Sample Source Codes)

----

## Variables are immutable by default

output of this code ?

```rust
fn main() {
  let x = 5;
  println!("1. The value of x is: {}", x);
  x = 6;
  println!("2. The value of x is: {}", x);
}
```

<!-- .element: class="fragment" -->[Playground][SSC1]

[SSC1]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=fn%20main()%20%7B%0A%20%20let%20x%20%3D%205%3B%0A%20%20println!(%221.%20The%20value%20of%20x%20is%3A%20%7B%7D%22%2C%20x)%3B%0A%20%20x%20%3D%206%3B%0A%20%20println!(%222.%20The%20value%20of%20x%20is%3A%20%7B%7D%22%2C%20x)%3B%0A%7D


----

## 0-cost abstraction

<table>
<thead><tr>
<th> 
[Rust][SSC4]
</th>
<th> 
[Go][SSC5]
</th>
</tr></thead>

<tbody><tr>
<td>
<div style="width:400px;overflow:auto;background-color:black">
<pre>
<code>
pub fn sum (n: i32) -> i32 {
  let mut sum = 0;
  for i in 1..n {
    sum += i;
  }
  sum
}
</code>
</pre>
</div>
</td>

<td>
<div style="width:400px;overflow:auto;background-color:black">
<pre>
<code>
package main

func sum (x int) int {
    sum := 0 
    for i := 1; i < x; i++{
        sum += i
    }
    return sum
}

func main() {}
</code>
</pre>
</div>
</td>

</tr></tbody>
</table>

[SSC4]: https://godbolt.org/z/oddjzPnbY
[SSC5]: https://godbolt.org/z/dcfvWqo67


----

### 0-cost abstraction

> 
> In Rust the previous code will be translated like following:
>
>> __(N-2)*(N-3)/2 + 2*N - 3 which can be simplified to N*(N-1)/2__
>
> The rust compiler was *smart enough* to replace the flow with the actual formula.
> 

----

## Strong type checking

C Language
```c
#include <stdio.h>
void main(){
  int x = 5;
  char y = 5;

  printf("x + y = %d", x + y);
}
```

[Rust][ST1]
```rust
fn main(){
   let x : i8 = 5;
   let y : i16 = 5;

   println!(" x + y = {}", x + y);
}
```

[ST1]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=fn%20main()%7B%0A%20%20%20let%20x%20%3A%20i8%20%3D%205%3B%0A%20%20%20let%20y%20%3A%20i16%20%3D%205%3B%0A%0A%20%20%20println!(%22%20x%20%2B%20y%20%3D%20%7B%7D%22%2C%20x%20%2B%20y)%3B%0A%7D


----

### Strong type checking

```bash
error[E0308]: mismatched types
--> <source>:6:32
|
6 | println!("x + y = {}", x + y);
| ^ expected i8, found i16

error[E0277]: cannot add `i16` to `i8`
--> <source>:6:30
|
6 | println!("x + y = {}", x + y);
| ^ no implementation for `i8 + i16`
|
= help: the trait `std::ops::Add<i16>` is not implemented for `i8`

error: aborting due to 2 previous errors
```
Some errors have detailed explanations: [E0277, E0308][ST2]. <br>
For more information about an error, try `rustc --explain E0277`. <br>
Compiler returned: 1
[ST2]: https://doc.rust-lang.org/error-index.html


----

## Ownership

a mechanism in the compiler that allows a single code entity to __own__ a dedicated piece of software. The code that owns the piece of software will set its __own lifetime__ to it.

```rust
struct A {
  x: i32,
}
 
fn own(a: A) {
  println!("I own 'a', and the value of x is {}", a.x);
}

fn main() {
  let a = A{x: 0};
  own(a);
  // As 'a' as been own by another function, the main function does not own 'a' anymore...
  println!("x is {}", a.x);
}
```

[Playground][OW1]<br>
[More about ownership][OW2]

[OW1]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=struct%20A%20%7B%0A%20%20x%3A%20i32%2C%0A%7D%0A%20%0Afn%20own(a%3A%20A)%20%7B%0A%20%20println!(%22I%20own%20'a'%2C%20and%20the%20value%20of%20x%20is%20%7B%7D%22%2C%20a.x)%3B%0A%7D%0A%0Afn%20main()%20%7B%0A%20%20let%20a%20%3D%20A%7Bx%3A%200%7D%3B%0A%20%20own(a)%3B%0A%20%20%2F%2F%20As%20'a'%20as%20been%20own%20by%20another%20function%2C%20the%20main%20function%20does%20not%20own%20'a'%20anymore...%0A%20%20println!(%22x%20is%20%7B%7D%22%2C%20a.x)%3B%0A%7D
[OW2]: https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html

----

### Ownership

Memory ownership, borrow checking & lifetimes

```bash
error[E0382]: borrow of moved value: `a`
--> <source>:13:23
|
10 | let a = A{x: 0};
| - move occurs because `a` has type `A`, which does not implement the `Copy` trait
11 | own(a);
| - value moved here
12 | // As 'a' as been own by another function, the main function does not own 'a' anymore...
13 | println!("x is {}", a.x);
| ^^^ value borrowed here after move

error: aborting due to previous error

For more information about this error, try `rustc --explain E0382`.
Compiler returned: 1 
```

----

### Ownership

Ownership ensures memory deallocation for a variable, 
<!-- .element: class="fragment" --> its prevent sharing of deallocated variable, 
<!-- .element: class="fragment" --> a single active binding to any heap allocated memory. 

<!-- .element: class="fragment" --> __All This happened at compile time!__

----

## Borrowing

A function will not __owns__ the variable but __borrows__ it, so the function is not responsible for the lifetime of the variable.

```rust
struct Disk {
  content: [u8; 5 ],
  title: String,
}

fn borrow (d: &Disk) {
  println! ("A user borrowed the Disk named '{}'", d.title);
}

fn main() {
  let d = Disk {content: [1u8, 2u8, 3u8, 4u8, 5u8], title:
  String::from("The Rustacean")};
  // A user borrows a disk...
  borrow (&d);
  //another user borrows a disk...
  borrow (&d);
}
```
[Run in playground][BO1]
[BO1]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=struct%20Disk%20%7B%0A%20%20content%3A%20%5Bu8%3B%205%20%5D%2C%0A%20%20title%3A%20String%2C%0A%7D%0A%0Afn%20borrow%20(d%3A%20%26Disk)%20%7B%0A%20%20println!%20(%22A%20user%20borrowed%20the%20Disk%20named%20'%7B%7D'%22%2C%20d.title)%3B%0A%7D%0A%0Afn%20main()%20%7B%0A%20%20let%20d%20%3D%20Disk%20%7Bcontent%3A%20%5B1u8%2C%202u8%2C%203u8%2C%204u8%2C%205u8%5D%2C%20title%3A%0A%20%20String%3A%3Afrom(%22The%20Rustacean%22)%7D%3B%0A%20%20%2F%2F%20A%20user%20borrows%20a%20disk...%0A%20%20borrow%20(%26d)%3B%0A%20%20%2F%2Fanother%20user%20borrows%20a%20disk...%0A%20%20borrow%20(%26d)%3B%0A%7D


----

### Borrowing

Can the borrowed variable be modified / updated?

```rust
fn main () {
  let s = String::from("Hello");
  change (&s);
}

fn change(some_string: &String) {
  some_string.push_str(", world");
}
```

[Run in playground][BO2]
<div class="fragment" />

[Run in playground with 'mut'][B03]

[BO2]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=fn%20main%20()%20%7B%0A%20%20let%20s%20%3D%20String%3A%3Afrom(%22Hello%22)%3B%0A%20%20change%20(%26s)%3B%0A%7D%0A%0Afn%20change(some_string%3A%20%26String)%20%7B%0A%20%20some_string.push_str(%22%2C%20world%22)%3B%0A%7D
[B03]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=fn%20main()%20%7B%0A%20%20%20%20let%20mut%20s%20%3D%20String%3A%3Afrom(%22hello%22)%3B%0A%20%20%20%20println!(%22%7B%7D%22%2Cs)%3B%0A%20%20%20%20change(%26mut%20s)%3B%0A%20%20%20%20println!(%22%7B%7D%22%2Cs)%3B%0A%7D%0A%0Afn%20change(some_string%3A%20%26mut%20String)%20%7B%0A%20%20%20%20some_string.push_str(%22%2C%20world%22)%3B%0A%7D

----

## Lifetimes

Lifetimes is the mechanism that tags a code entity with a life scope, to ensures that every reference to this entity __shares the same lifetime__.

```rust
fn main() {
  {
    let r;
    {
      let x = 5;
      r = &x;
      println!("1. print r: {}", r);
    }
    println!("2. print r: {}", r);
  }
}
```
[Run in playground] [LF1]
[LF1]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=fn%20main()%20%7B%0A%20%20%7B%0A%20%20%20%20let%20r%3B%0A%20%20%20%20%7B%0A%20%20%20%20%20%20let%20x%20%3D%205%3B%0A%20%20%20%20%20%20r%20%3D%20%26x%3B%0A%20%20%20%20%20%20println!(%221.%20print%20r%3A%20%7B%7D%22%2C%20r)%3B%0A%20%20%20%20%7D%0A%20%20%20%20println!(%222.%20print%20r%3A%20%7B%7D%22%2C%20r)%3B%0A%20%20%7D%0A%7D

----

### Lifetimes

The main aim of lifetimes is to prevent dangling references, which cause a program to reference data other than the data it’s intended to reference.

```bash
error[E0597]: `x` does not live long enough
 --> src/main.rs:6:11
  |
6 |       r = &x;
  |           ^^ borrowed value does not live long enough
7 |       println!("1. print r: {}", r);
8 |     }
  |     - `x` dropped here while still borrowed
9 |     println!("2. print r: {}", r);
  |                                - borrow later used here
```

More samples https://doc.rust-lang.org/book/ch10-03-lifetime-syntax.html and https://doc.rust-lang.org/1.9.0/book/lifetimes.html 


----

### Memory Safety

Compile time memory safety check
* Preventing type errors at compile time
* Preventing memory errors at compile time
  * uninitialized variable
  * invalid memory access
  * dangling pointer/reference/variable
  * Incorrectly using a moved object
  * Data race in multithreading
* Safe Rust guarantees an absence of data races, which defined as:
  * Two or more threads concurrently accessing a location of memory
  * One or more of them is a write
  * One or more of them is unsynchronized <br>
*However, rust doesn’t prevent general race conditions*

----  ----

# 3. RUST Common Vulnerabilities

----

## Common Programming Mistakes

* Unwrapping, panic, arithmetic, UTF-8, out of bound errors
* Stack overflow, resource exhaustion bugs

Advanced vulnerabilites (*unsafe*)
* Undefined behavior
* use after free (ManuallyDrop)
* memory errors

In most cases if it hit those vulnerabilities, it will trigger panic!

----

## Unwrapping

Unwrap will either return the inner element or panic when there is no element was found.
```rust
// The commoner has seen it all, and can handle any gift well.
// All gifts are handled explicitly using `match`.
fn give_commoner(gift: Option<&str>) {
    // Specify a course of action for each case.
    match gift {
        Some("snake") => println!("Yuck! I'm putting this snake back in the forest."),
        Some(inner)   => println!("{}? How nice.", inner),
        None          => println!("No gift? Oh well."),
    }
}

// Our sheltered royal will `panic` at the sight of snakes.
// All gifts are handled implicitly using `unwrap`.
fn give_royal(gift: Option<&str>) {
    // `unwrap` returns a `panic` when it receives a `None`.
    let inside = gift.unwrap();
    if inside == "snake" { panic!("AAAaaaaa!!!!"); }

    println!("I love {}s!!!!!", inside);
}
```

----

### Unwrapping

```rust
fn main() {
  let food  = Some("cabbage");
  let snake = Some("snake");
  let void  = None;

  give_commoner(food);
  give_commoner(snake);
  give_commoner(void);

  let bird = Some("robin");
  let nothing = None;

  give_royal(bird);
  give_royal(nothing);
}
```
[Run in playground][UW1]
[UW1]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=%2F%2F%20The%20commoner%20has%20seen%20it%20all%2C%20and%20can%20handle%20any%20gift%20well.%0A%2F%2F%20All%20gifts%20are%20handled%20explicitly%20using%20%60match%60.%0Afn%20give_commoner(gift%3A%20Option%3C%26str%3E)%20%7B%0A%20%20%20%20%2F%2F%20Specify%20a%20course%20of%20action%20for%20each%20case.%0A%20%20%20%20match%20gift%20%7B%0A%20%20%20%20%20%20%20%20Some(%22snake%22)%20%3D%3E%20println!(%22Yuck!%20I'm%20putting%20this%20snake%20back%20in%20the%20forest.%22)%2C%0A%20%20%20%20%20%20%20%20Some(inner)%20%20%20%3D%3E%20println!(%22%7B%7D%3F%20How%20nice.%22%2C%20inner)%2C%0A%20%20%20%20%20%20%20%20None%20%20%20%20%20%20%20%20%20%20%3D%3E%20println!(%22No%20gift%3F%20Oh%20well.%22)%2C%0A%20%20%20%20%7D%0A%7D%0A%0A%2F%2F%20Our%20sheltered%20royal%20will%20%60panic%60%20at%20the%20sight%20of%20snakes.%0A%2F%2F%20All%20gifts%20are%20handled%20implicitly%20using%20%60unwrap%60.%0Afn%20give_royal(gift%3A%20Option%3C%26str%3E)%20%7B%0A%20%20%20%20%2F%2F%20%60unwrap%60%20returns%20a%20%60panic%60%20when%20it%20receives%20a%20%60None%60.%0A%20%20%20%20let%20inside%20%3D%20gift.unwrap()%3B%0A%20%20%20%20if%20inside%20%3D%3D%20%22snake%22%20%7B%20panic!(%22AAAaaaaa!!!!%22)%3B%20%7D%0A%0A%20%20%20%20println!(%22I%20love%20%7B%7Ds!!!!!%22%2C%20inside)%3B%0A%7D%0A%0Afn%20main()%20%7B%0A%20%20%20%20let%20food%20%20%3D%20Some(%22cabbage%22)%3B%0A%20%20%20%20let%20snake%20%3D%20Some(%22snake%22)%3B%0A%20%20%20%20let%20void%20%20%3D%20None%3B%0A%0A%20%20%20%20give_commoner(food)%3B%0A%20%20%20%20give_commoner(snake)%3B%0A%20%20%20%20give_commoner(void)%3B%0A%0A%20%20%20%20let%20bird%20%3D%20Some(%22robin%22)%3B%0A%20%20%20%20let%20nothing%20%3D%20None%3B%0A%0A%20%20%20%20give_royal(bird)%3B%0A%20%20%20%20give_royal(nothing)%3B%0A%7D%0A 


----

## Panic

The cause of error should be available, and generic errors should be avoided.
Common patterns that can cause panics are:
* Using unwrap or expect
* Using assert
* Unchecked access to an array
* Integer overflow (in debug mode)
* Division by zero
* Large allocations 
* String formatting using format!.

<div class="fragment" />
> __*ANSSI programming guideline Rule 14*__: Don’t use functions that can cause panic! Functions or instructions that can cause the code to panic at runtime must not be used.

----

## Panic

List of panicking macros in std library:

* __`panic!`__ – panics the current thread
* __`unimplemented!`__ – indicates unimplemented code by panicking with a message of “not implemented”.
* __`unreachable!`__ – indicates unreachable code
* __`assert!`__, __`assert_eq!`__, __`assert_ne!`__ – asserts that some expressions are respected at runtime
* __`debug_assert!`__, __`debug_assert_eq!`__, __`debug_assert_ne!`__ – activated only in debug mode. i.e. debug-assertions flag given to the compiler
* __`todo!`__ – indicates unfinished code.

----

### Panic

Detection of panic macros

* <!-- .element: class="fragment" --> Simple regex (“__`panic!`__”, "__`*assert.*`__”, “__`unreachable`__”, etc)
* <!-- .element: class="fragment" --> __`#[no_panic]`__ compiler macro
* <!-- .element: class="fragment" --> Clippy macros: __`#[warn(clippy::unimplemented)]`__

----

## Arithmetic Errors

<table>
<thead><tr>
<th> 
[Division by zero][AE1]
</th>
<th> 
[Integer Overflow/ <br /> Underflow][AE2]
</th>
<th> 
[Casting Overflow][AE3]
</th>
</tr></thead>

<tbody><tr>
<td>
<div style="width:300px;overflow:auto;background-color:black">
<pre>
<code>
fn main(){
  let a = 4;
  let b = 0; 
  let _c = a/b;
}
</code>
</pre>
</div>
</td>

<td>
<div style="width:300px;overflow:auto;background-color:black">
<pre>
<code>
let x : u8 = 255;
let y : u8 = 1;
let _z = x + y;
</code>
</pre>
</div>
</td>

<td>
<div style="width:300px;overflow:auto;background-color:black">
<pre>
<code>
fn main() {
  let x : u16 = 266;
  let b = x as u8;
  println! ("{}", b)
}
</code>
</pre>
</div>
</td>

</tr></tbody>
</table>

[AE1]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=fn%20main()%7B%0A%20%20let%20a%20%3D%204%3B%0A%20%20let%20b%20%3D%200%3B%20%0A%20%20let%20_c%20%3D%20a%2Fb%3B%0A%7D
[AE2]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=fn%20main()%7B%0A%20%20let%20x%20%3A%20u8%20%3D%20255%3B%0A%20%20let%20y%20%3A%20u8%20%3D%201%3B%0A%20%20let%20_z%20%3D%20x%20%2B%20y%3B%0A%7D
[AE3]: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&code=fn%20main()%20%7B%0A%20%20let%20x%20%3A%20u16%20%3D%20266%3B%0A%20%20let%20b%20%3D%20x%20as%20u8%3B%0A%20%20println!%20(%22%7B%7D%22%2C%20b)%0A%7D


----

### Arithmetic Errors
How to detect those potential arithmetic issues :
* Manual source code review
* Compilation Time : direct ‘div by 0’ can be detected immediately
* Use of __`overflowing_<op>`__, __`wrapping_<op>`__ or __'Wrapping`__ type.
* Compilation with __`overflow-checks`__
* Lint arithmetic_overflow (__`#![deny(arithmetic_overflow)]`__) it is by default.
* Clippy __`overflow_check_conditional`__
* Lint __`#![deny(overflowing_literals)]`__ it is by default.

<div class="fragment" />
> __*ANSSI Rule LANG-ARITH*__<br>
> When assuming that an arithmetic operation can produce an overflow, the specialized functions overflowing_<op>, wrapping_<op>, 
> or the Wrapping type must be used.

----

### Arithmetic Errors

Even some lint is set to detect the overflow, it may be missed when it is using data type at the end of the value.

[Casting - Rust By Example (rust-lang.org)] [AE4]

Try to change the lint from allow become deny in the lint (there will be error as it detects overflow).
Now try to add data types (u32 or u64) at the back of the number. 1000 -> 1000u64.
It will not detect the overflow.

[AE4]: https://doc.rust-lang.org/stable/rust-by-example/types/cast.html#casting

----

## UTF-8 strings handling

It will trigger panic at runtime when it is trying to access character not in a boundary of utf-8.

```rust
fn main() {
    let x: &str = "банан";
    let y = &x[..3];
    println!("{}", y);
}
```

> thread 'main' panicked at 'byte index 3 is not a char boundary; it is inside 'а' (bytes 2..4) of `банан`', src/main.rs:3:14

----

## Index out of bounds

Try to access an element of an array that is beyond of the array.
No compilation errors but runtime panic error.

```rust
fn main() {
  let a = vec![1,2,3,4,5];
  let offset = 10;
  println!("{:?}", a[offset]);
}
```

> thread 'main' panicked at 'index out of bounds: the len is 5 but the index is 10', src/main.rs:4:22

----

## Index out of bounds (unsafe)

Try to access an element of an array that is beyond of the array using unsafe code block.

```rust
fn main() {
  let xs = [0, 1, 2, 3];
  let y = unsafe {*xs.as_ptr().offset(4)};
  println!("{:?}", y);
}
```
There should be compilation error, right?<br> <!-- .element: class="fragment" -->
a runtime panic error, perhaps?<br> <!-- .element: class="fragment" -->
any guess? <!-- .element: class="fragment" -->

----

### Index out of bounds (unsafe)

!!! NO compilation errors NO runtime panic error!!!

```sh
OUTPUT cargo run
 Finished dev [unoptimized + debuginfo] target(s) in 0.46s
     Running `target/debug/oobunsafe`

22003
``` 
<!-- .element: class="fragment" --> 

and the returned value can be varied <!-- .element: class="fragment" --> 

----

### Index out of bounds (unsafe)

Was that output expected!? <br>
Now lets run it with MIRI (Mid-level Intermediate Representation Interpreter)<br>

Output from MIRI 
```sh
error: Undefined Behavior: memory access failed: pointer must be in-bounds at offset 20, but is outside bounds of alloc1350 which has size 16
 --> src/main.rs:3:22
  |
3 |     let _y = unsafe {*_xs.as_ptr().offset(4)};
  |                      ^^^^^^^^^^^^^^^^^^^^^^^ memory access failed: pointer must be in-bounds at offset 20, but is outside bounds of alloc1350 which has size 16
  |
  = help: this indicates a bug in the program: it performed an invalid operation, and caused Undefined Behavior 
... 
...
```
<!-- .element: class="fragment" --> 

----

## Memory Leak
The allocated memory is not reachable and could not claimed
`drop`: simply triggers an early memory reclamation that calls associated destructors when needed.
`forget`: skips any call to the destructors.

```rust
use std::mem; 
fn main() {
  let xs = vec![0, 1, 2, 3];
  mem::forget(xs);
}
```

<div class="fragment" />
> __*Rule MEM-FORGET*__
> In a secure Rust development, the forget function of `std::mem` (`core::mem`) must not be used.

----

### Memory Leak

OUTPUT from MIRI
```sh
 Running `/home/vht/.rustup/toolchains/nightly-2021-05-22-x86_64-unknown-linux-gnu/bin/cargo-miri target/x86_64-unknown-linux-gnu/debug/memleak`
The following memory was leaked: alloc1358 (Rust heap, size: 16, align: 4) {
    00 00 00 00 01 00 00 00 02 00 00 00 03 00 00 00 │ ................
}

error: the evaluated program leaked memory

error: aborting due to previous error
```

> __*Notes:*__
> The output has not mentioning about the variable or line of code that leaking…so, how to find the exact variable or line of code that has memory leak? Map file to alloc1358?

----

## Stack Overflow

The most common cause of stack overflow is excessively deep or infinite recursion

```rust 
fn recursive_func (x: i32) -> i32 { 
    if x > 0 {
        recursive_func(x);
    }
    return x;
}

fn main() {
    let a = 1;
    let _b = recursive_func(a);
    println!("{}",_b);
}
```

----

### Stack Overflow

Output from Cargo Run 
Runtime Error

```sh
thread 'main' has overflowed its stack
fatal runtime error: stack overflow
Aborted
```

> NO clear error identification, it is hard to spot the root cause of error in the code. <br> 
> Even in MIRI the error output does not give much information.

----

## Use After Free

Error occurs when a program continue to use pointer after it has been freed or deleted.

```rust
fn main() {
  let xs = vec![0, 1, 2, 3];
  let y = xs.as_ptr();
  drop(xs);
  let z = unsafe { *y };
  println!("{}",z);
}
```

----

### Use After Free

MIRI Error Output

```sh
error: Undefined Behavior: pointer to alloc1364 was dereferenced after this allocation got freed
 --> src/main.rs:5:22
  |
5 |     let z = unsafe { *y };
  |                      ^^ pointer to alloc1364 was dereferenced after this allocation got freed
```

----

## Uninitialized Memory

Uninitialized memory is used to declared variables without assigning value.

```rust
use std::mem::{MaybeUninit};

fn main() {
  let xs: [u8; 3] = unsafe { MaybeUninit::uninit().assume_init() };
  let y = xs[0] + xs[1] + xs[2];
  println!("{:?}", y);
}
```

----

### Uninitialized Memory

MIRI Error Output
```sh
error: Undefined Behavior: using uninitialized data, but this operation requires initialized memory
 --> src/main.rs:5:13
  |
5 |     let y = xs[0] + xs[1] + xs[2];
  |             ^^^^^^^^^^^^^ using uninitialized data, but this operation 
requires initialized memory
  |
```

----

## Data Race

Race condition is a flaw that occurs when timing or ordering of events affects a program’s correctness.

```rust
use std::thread;
static mut ANSWER: i32 = 0;

fn main() {
  let t1 = thread::spawn(|| unsafe { ANSWER = 42 });
  let t2 = thread::spawn(|| unsafe { ANSWER = 24 });
  let t3 = thread::spawn(|| unsafe { ANSWER = 142 });
  t1.join().ok();
  t2.join().ok();
  t3.join().ok();
  unsafe {
    println!("{}", ANSWER);
  }
}
```

> Note: Run this program multiple times.

----

### Data Race

MIRI Error Output
```sh
error: Undefined Behavior: Data race detected between Write on Thread(id = 2) and Write on Thread(id = 1), memory(alloc0,offset=0,size=4)
(current vector clock = VClock([16, 0, 6]), conflicting timestamp = VClock([0, 6]))
 --> src/main.rs:7:40
  |
7 |     let t2 = thread::spawn(|| unsafe { ANSWER = 24 });
  |                                        ^^^^^^^^^^^ Data race detected between Write on Thread(id = 2) and Write on Thread(id = 1), memory(alloc0,offset=0,size=4)
(current vector clock = VClock([16, 0, 6]), conflicting timestamp = VClock([0, 6]))
  |
```

> Note/Reminder: Rust cannot guarantee the safety of general race condition of your program.

----  ----

# 4. Rust Common Auditing Tools 

----

## cargo audit

It audits the dependencies used in the project by using the cargo.lock file as input.

More details: https://github.com/RustSec/rustsec/tree/main/cargo-audit 

----

## MIRI

An experimental interpreter for Rust's mid-level intermediate representation (MIR). <br> 
It can run binaries and test suites of cargo projects and detect certain classes of undefined behavior, <br>
including Rust-specific ones that sanitizers cannot detect. <br>
Moreover, it can do this for arbitrary CPU architectures independent of the host ("cross-interpretation").

More details: https://github.com/rust-lang/miri

----

## RUST Sanitizers

Controlled using the compiler's -Z flag

https://doc.rust-lang.org/nightly/unstable-book/compiler-flags/sanitizer.html


[RUSTFLAGS="-Z sanitizer=memory“][RS1] <br>
[RUSTFLAGS="-Z sanitizer=leak“][RS2] <br>
[RUSTFLAGS="-Z sanitizer=address“][RS3] <br>
[RUSTFLAGS="-Z sanitizer=thread“][RS4] <br>

> This flag will allow you to set unstable options of rustc. In order to set multiple options, the -Z flag can be used multiple times. For example: rustc -Z verbose -Z time. Specifying options with -Z is only available on nightly. To view all available options run: rustc -Z help.
> -- <cite> https://doc.rust-lang.org/rustc/command-line-arguments.html </cite>

[RS1]: https://clang.llvm.org/docs/MemorySanitizer.html
[RS2]: https://clang.llvm.org/docs/LeakSanitizer.html
[RS3]: https://clang.llvm.org/docs/AddressSanitizer.html
[RS4]: https://clang.llvm.org/docs/ThreadSanitizer.html


----  ----

# Discussions & QnA

----  ----

<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

# Thank You!
<!-- .element: class="no-toc-progress" -->

